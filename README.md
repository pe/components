# DVC CI Components

DVC GitLab ci components.

[[_TOC_]]


## Components

### deploy

Component for demonstration purposes.

```yaml
include:
  - component: gitlab.opencode.de/pe/components/deploy@<VERSION>
    inputs:
      stage:
        default: deploy
      environment:
        default: staging

```

where `<VERSION>` is the latest released tag or `main`.

This will add a `deploy`job to the pipeline.

| Input | Default value | Description |
| ----- | ------------- | ----------- |
| `stage`  |  | The pipeline stage |
| `environment`     |  | The pipeline environment |
